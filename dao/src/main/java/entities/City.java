package entities;

import java.util.Set;

public class City {

    private int id;
    private String latitude;
    private String longitude;
    private String name;
    private String country;
    private String gmtOffset;
    private String dstOffset;

    private Set<Flight> departureFlights;
    private Set<Flight> arrivalFlights;

    public City() {}

    public City(int id, String latitude, String longitude, String name, String country, String gmtOffset, String dstOffset) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.country = country;
        this.gmtOffset = gmtOffset;
        this.dstOffset = dstOffset;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGmtOffset() {
        return gmtOffset;
    }

    public void setGmtOffset(String gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public String getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(String dstOffset) {
        this.dstOffset = dstOffset;
    }

    public Set<Flight> getDepartureFlights() {
        return departureFlights;
    }

    public void setDepartureFlights(Set<Flight> departureFlights) {
        this.departureFlights = departureFlights;
    }

    public Set<Flight> getArrivalFlights() {
        return arrivalFlights;
    }

    public void setArrivalFlights(Set<Flight> arrivalFlights) {
        this.arrivalFlights = arrivalFlights;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", gmtOffset='" + gmtOffset + '\'' +
                ", dstOffset='" + dstOffset + '\'' +
                ", departureFlights=" + departureFlights +
                ", arrivalFlights=" + arrivalFlights +
                '}';
    }
}
