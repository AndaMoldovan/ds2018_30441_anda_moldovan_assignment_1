package entities;

import java.sql.Date;
import java.sql.Timestamp;

public class Flight {

    private int id;
    private String flightNumber;
    private City departureCity;
    private Timestamp departureDate;
    private City arrivalCity;
    private Timestamp arrivalDate;
    private String airplaneType;

    public Flight() {}

    public Flight(int id, String flightNumber, City departureCity, Timestamp departureDate, City arrivalCity, Timestamp arrivalDate, String airplaneType) {
        this.id = id;
        this.flightNumber = flightNumber;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
        this.airplaneType = airplaneType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Timestamp getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightNumber='" + flightNumber + '\'' +
                ", departureCity=" + departureCity +
                ", departureDate=" + departureDate +
                ", arrivalCity=" + arrivalCity +
                ", arrivalDate=" + arrivalDate +
                ", airplaneType='" + airplaneType + '\'' +
                '}';
    }
}
