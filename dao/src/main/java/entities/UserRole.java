package entities;

import java.util.Set;

public class UserRole {

    private int id;
    private String role;

    private Set<User> users;


    public UserRole() {}

    public UserRole(int id, String role) {
        this.id = id;
        this.role = role;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString(){
        return "User_Role: id= " + id + " role= " + role + "\n";
    }
}
