package dao;

import java.util.List;
import entities.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;

public class CityDAO {

    private SessionFactory factory;

    public CityDAO(SessionFactory factory) {this.factory = factory;}

    public City getCityById(int id){
        Session session = factory.openSession();

        City city = null;

        try{
            Transaction tx = session.beginTransaction();

            city = session.get(City.class, id);
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
        return city;
    }

    public List<City> getAllCities(){
        Session session = factory.openSession();
        List<City> cities = new ArrayList<City>();
        try{
            Transaction tx = session.beginTransaction();

            cities = session.createQuery("FROM City").list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }

        return cities;
    }

    public void addCity(City city){
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();

            session.save(city);
            tx.commit();
            System.out.println("City successfully added to the database");
        }catch(HibernateException e) {
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void updateCity(City city){
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();

            session.update(city);
            tx.commit();
            System.out.println("City successfully updated the database");
        }catch(HibernateException e) {
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void deleteCity(int id){
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();

            City city = getCityById(id);
            session.delete(city);
            tx.commit();
            System.out.println("City successfully deleted from the database");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

}
