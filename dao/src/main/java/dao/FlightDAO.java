package dao;

import entities.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class FlightDAO {
    private SessionFactory factory;

    public FlightDAO(SessionFactory factory) {this.factory = factory;}

    public Flight getFlightById(int id){
        Session session = factory.openSession();

        Flight flight = null;
        try{
            Transaction tx = session.beginTransaction();

            flight = session.get(Flight.class, id);
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }

        return flight;
    }

    public List<Flight> getAllFlights(){
        List<Flight> flights = new ArrayList<>();
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            session.close();
        }
        return flights;
    }

    public void addFlight(Flight flight){
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();

            session.save(flight);
            tx.commit();
            System.out.println("Flight added to the database");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void updateFlight(Flight flight){
        Session session = factory.openSession();
        try{
            Transaction tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
            System.out.println("Flight updated in the database");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void deleteFlight(int id){
        Session session = factory.openSession();
        try{
            Transaction tx = session.beginTransaction();
            Flight flight = getFlightById(id);
            session.delete(flight);
            tx.commit();
            System.out.println("Flight successfully deleted from the database");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }
}
