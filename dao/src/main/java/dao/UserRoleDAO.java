package dao;

import entities.UserRole;
import org.hibernate.*;

public class UserRoleDAO {

    private SessionFactory factory;

    public UserRoleDAO(SessionFactory factory) {this.factory = factory;}

    public UserRole getUserRole(int id){
        Session session = factory.openSession();
        Transaction tx = null;

        UserRole userRole = null;

       try{
           tx = session.beginTransaction();
           userRole = (UserRole) session.get(UserRole.class, id);
           tx.commit();
       }catch(Exception e){
           e.printStackTrace();
       }finally{
           session.close();
       }

       return userRole;
    }

    public void addUserRole(String role){
        Session session = factory.openSession();
        try{
            Transaction tx = session.beginTransaction();

            UserRole ur = new UserRole();
            ur.setRole(role);
            session.save(ur);
            tx.commit();
            System.out.println("UserRole saved to the db \n");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }
}
