package dao;

import entities.User;
import org.hibernate.HibernateException;
import org.hibernate.*;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private SessionFactory factory;

    public UserDAO(SessionFactory factory) {this.factory = factory;}

    public User getUserByID(int id){
        Session session = factory.openSession();
        Transaction tx = null;

        User user = null;

        try{
            tx = session.beginTransaction();
            user = (User) session.get(User.class, id);
            tx.commit();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            session.close();
        }

        return user;
    }

    public List<User> getAllUSers(){
        Session session = factory.openSession();

        List<User> users = new ArrayList<User>();

        try{
            Transaction tx = session.beginTransaction();

            users =  session.createQuery("FROM User").list();
            tx.commit();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            session.close();
        }
        return users;
    }

    public void addUser(User user){
        Session session = factory.openSession();
        try{
            Transaction tx = session.beginTransaction();

            session.save(user);
            tx.commit();
            System.out.println("User successfully saved to the database");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void updateUser(User user){
        Session session = factory.openSession();

        try{
            Transaction tx = session.beginTransaction();

            session.update(user);
            tx.commit();
            System.out.println("User updated");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

    public void deleteUser(int id){
        Session session = factory.openSession();
        try{
            Transaction tx = session.beginTransaction();

            User user = getUserByID(id);
            session.delete(user);
            tx.commit();
            System.out.println("User deleted");
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }

}
