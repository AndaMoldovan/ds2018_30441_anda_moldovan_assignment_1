package filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationFilter implements Filter {

    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        HttpSession httpSession = req.getSession(false);
        boolean auth = false;
        String role = null;

//        if(httpSession == null && (((HttpServletRequest) servletRequest).getRequestURI().endsWith("login"))){
//            res.sendRedirect("/login.html");
//            return;
//        }

        if(httpSession != null){
            role = (String) httpSession.getAttribute("role");
            auth = true;
        }
       // System.out.println("\n\n\n\n\n\n\nRole: " +email + "  " + role + " \n\n\n\n\n\n\n\n");


        if(auth){
            if(role != null){
                if((req.getRequestURI().endsWith("admin") && role.equals("USER")) ||
                        (req.getRequestURI().endsWith("user") && role.equals("ADMIN"))){
                    res.sendRedirect("/404.html");
                    return;
                }else{
                    filterChain.doFilter(servletRequest, servletResponse);
                }
            }else{
                res.sendRedirect("/login");
                return;
            }
        }else if (filterConfig != null) {
            res.sendRedirect("/404.html");
            return;
        }
//        throw new ServletException
//                ("Unauthorized access, unable to forward to login page");

    }

    @Override
    public void destroy() {

    }
}
