package filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthentificationFilter implements Filter {

    private ServletContext context;
    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        HttpSession httpSession = req.getSession(false);
        boolean auth = false;

//        if(httpSession == null && (((HttpServletRequest) servletRequest).getRequestURI().endsWith("login"))){
//            res.sendRedirect("/login.html");
//            return;
//        }

        if(httpSession != null){
            String userEmail = (String) httpSession.getAttribute("email");
            auth = true;
        }

        if(auth){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } else if (filterConfig != null) {
            String login_page = filterConfig.getInitParameter("login_page");
            if (login_page != null && !"".equals(login_page)) {
                filterConfig.getServletContext().getRequestDispatcher(login_page).
                        forward(servletRequest, servletResponse);
                return;
            }
        }
//        if(req.getServletPath().endsWith("/login")){
//            filterChain.doFilter(req, res);
//            return;
//        }
//
//        if(userEmail != null){
//            filterChain.doFilter(req, res);
//            return;
//        }

      //  res.sendRedirect("login");
//        throw new ServletException
//                ("Unauthorized access, unable to forward to login page");
    }

    @Override
    public void destroy() {

    }
}
