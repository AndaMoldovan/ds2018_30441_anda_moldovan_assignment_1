import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.util.List;

import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import org.hibernate.cfg.Configuration;
import services.CityService;
import services.FlightService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class AdminServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String html = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader("D:\\Users\\andam\\Documents\\Anul IV\\DS\\DS2018_MoldovanAnda_Assignment1\\presentation\\src\\main\\admin.html"));
            String str;
            while((str = br.readLine()) != null){
                html += str;
                System.out.println(str);
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        out.println(html);

        out.println("<font color=red>Hello Admin </font><br><br>");

       // String postForm = ""

        out.println("<h1> Cities Table </h1>");

        CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory()) ;
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        FlightService flightService = new FlightService();
        List<City> cities = cityDAO.getAllCities();
        List<Flight> flights = flightDAO.getAllFlights();

        String cityTable = "<form action=\"/admin\" method=\"post\">" +
                "<table data-toggle=\"table\" >\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Id</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Latitude</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Longitude</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">City Name</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Country Name</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Winter Hour (GMT)</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Summer Hour (DST)</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\"> # </th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n";

        for(City city : cities){
            cityTable += "<tr>";
            cityTable += "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getId() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getLatitude() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getLongitude() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getName() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getCountry() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getGmtOffset() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + city.getDstOffset() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;padding-left: 23px;padding-right: 10px;\"> <input type=\"submit\" name=\"submit\" value=\"Delete City " + city.getId() + "\" /></td>\n" +
                    "</tr>\n";
        }
        cityTable += "</tbody>\n </table>\n </form>";
        out.println(cityTable);

        String cityForm = "<form action=\"/admin\" method=\"post\">" +
                "Latitude: <input type=\"text\" name=\"latitude\" style=\"margin: 10px; display: inline-block; text-align: left;\"/><br>" +
                "Longitude: <input type=\"text\" name=\"longitude\" style=\"margin: 10px; display: inline-block; text-align: left;\"/><br>" +
                "City Name: <input type=\"text\" name=\"cityname\" style=\"margin: 10px; display: inline-block; text-align: left;\"/><br>" +
                "<input type=\"submit\" name=\"submit\" value=\"Add City\" style=\"margin-left: 90px;\"/>" +
                "<input type=\"submit\" name=\"submit\" value=\"Delete City\" style=\"margin-left: 10px;\"/>" +
                "</form>";

        out.println(cityForm);

        out.println("<br>\n <br>\n <h1> Flights Table </h1>");

        String flightTable =  "<form action=\"/admin\" method=\"post\">" +
                "<table data-toggle=\"table\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Id</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Flight Number</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Departure City</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Departure Date</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Departure Time</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Arrival City</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Arrival Date</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Arrival Time</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Airplane Type</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\"> # </th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\"> # </th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n";


        for(Flight flight: flights){
            flightTable += "<tr>";
            flightTable += "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getId() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getFlightNumber() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getDepartureCity().getName() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flightService.getDateAndTime(flight.getDepartureDate().toString())[0] + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flightService.getDateAndTime(flight.getDepartureDate().toString())[1] + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getArrivalCity().getName() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flightService.getDateAndTime(flight.getArrivalDate().toString())[0] + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flightService.getDateAndTime(flight.getArrivalDate().toString())[1] + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getAirplaneType() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\"> <input type=\"submit\" name=\"submit\" value=\"Update Flight " + flight.getId() + "\" /> </td>" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;padding-left: 23px;padding-right: 10px;\"> <input type=\"submit\" name=\"submit\" value=\"Delete Flight " + flight.getId() + "\" /> </td>" +
                    "</tr>\n";
        }

        flightTable += "</body>\n </table> \n ";
       // out.println(flightTable);

//        String flightForm = "<form action=\"/admin\" method=\"post\">" +
//                "Departure City: <input type=\"text\" name=\"departurecity\" /> <br>" +
//                "Departure Date: <input type=\"text\" name=\"departuredate\" /> <br>" +
//                "Departure Time: <input type=\"text\" name=\"departuretime\" /> <br>" +
//                "Arrival City: <input type=\"text\" name=\"arrivalcity\" /> <br>" +
//                "Flight Time: <input type=\"text\" name=\"flighttime\" /> <br>" +
//                "Airplane Type: <input type=\"text\" name=\"airplanetype\" /> <br>" +
//                "Flight Number: <input type=\"text\" name=\"airplanenumber\" /> <br>" +
//                "<input type=\"submit\" name=\"submit\" value=\"Add Flight\">" +
//                "</form>";
        flightTable += "<br>" +
                "Departure City: <input type=\"text\" name=\"departurecity\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Departure Date: <input type=\"text\" name=\"departuredate\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Departure Time: <input type=\"text\" name=\"departuretime\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Arrival City: <input type=\"text\" name=\"arrivalcity\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Flight Time: <input type=\"text\" name=\"flighttime\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Airplane Type: <input type=\"text\" name=\"airplanetype\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "Flight Number: <input type=\"text\" name=\"airplanenumber\" style=\"margin: 10px; display: inline-block; text-align: left;\"/> <br>" +
                "<input type=\"submit\" name=\"submit\" value=\"Add Flight\" style=\"margin-left: 90px;\">" +
                "</form>";

        out.println(flightTable);

        out.println("</body>\n" +
                "</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CityService cityService = new CityService(new CityDAO(new Configuration().configure().buildSessionFactory()));


        if(request.getParameter("submit").equals("logout")){
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("/login");
        }else
        if(request.getParameter("submit").equals("Add City")){
            String lat = request.getParameter("latitude");
            String lng = request.getParameter("longitude");
            String cityName = request.getParameter("cityname");

            cityService.addCity(lat, lng, cityName);
            System.out.println("\n\n\n\n add city \n\n\n\n\n");
            doGet(request, response);
        }else if(request.getParameter("submit").contains("Delete City")){
                int id = Integer.parseInt(request.getParameter("submit").split(" ")[2]);
                cityService.deleteCity(id);
                PrintWriter out = response.getWriter();
                String alert = "<script> alert(\"City deleted successfully from the database\")</script>";
                out.println(alert);
                doGet(request, response);
        }else if(request.getParameter("submit").equals("Add Flight")){
            String departureCity = request.getParameter("departurecity");
            String departureDate = request.getParameter("departuredate");
            String departureTime = request.getParameter("departuretime");
            String arrivalCity = request.getParameter("arrivalcity");
            String flightTime = request.getParameter("flighttime");
            String airplaneType = request.getParameter("airplanetype");
            String airplaneNumber = request.getParameter("airplanenumber");

            FlightService flightService = new FlightService();
            try {
                flightService.createFlight(departureCity, departureDate, departureTime, arrivalCity, flightTime, airplaneType, airplaneNumber);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            PrintWriter out = response.getWriter();
            String alert = "<script> alert(\"Flight added to the database\")</script>";
            out.println(alert);

            doGet(request, response);
        }else if(request.getParameter("submit").contains("Update Flight")){
            PrintWriter out = response.getWriter();
            String alert;

            int flightId = Integer.parseInt(request.getParameter("submit").split(" ")[2]);
            String departureCity = request.getParameter("departurecity");
            String departureDate = request.getParameter("departuredate");
            String departureTime = request.getParameter("departuretime");
            String arrivalCity = request.getParameter("arrivalcity");
            String flightTime = request.getParameter("flighttime");
            String airplaneType = request.getParameter("airplanetype");
            String airplaneNumber = request.getParameter("airplanenumber");

            if(departureTime.equals("") && (!departureDate.equals("") || flightTime.equals("")) ){
                alert = "<script> alert(\"You have to add the new date, new time and new flight time in order to continue\")</script>";
                out.println(alert);
                doGet(request, response);
            }

            FlightService flightService = new FlightService();
            try {
                flightService.updateFlight(flightId, departureCity, departureDate, departureTime, arrivalCity, flightTime, airplaneType, airplaneNumber);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            alert = "<script> alert(\"Flight successfully updated the database\")</script>";
            out.println(alert);

            doGet(request, response);
        }else if(request.getParameter("submit").contains("Delete Flight")){
            PrintWriter out = response.getWriter();
            String alert;

            int flightId = Integer.parseInt(request.getParameter("submit").split(" ")[2]);

            FlightService flightService = new FlightService();
            flightService.deleteFlight(flightId);

            alert = "<script> alert(\"Flight successfully deleted from the database\")</script>";
            out.println(alert);
            doGet(request, response);
        }
    }
}
