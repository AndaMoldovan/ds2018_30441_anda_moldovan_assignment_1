import dao.UserDAO;
import org.hibernate.cfg.Configuration;
import services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class ServletTest extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UserService userService = new UserService(new UserDAO(new Configuration().configure().buildSessionFactory()));
        if (userService.validateLogin(username, password).equals("USER")) {
//            RequestDispatcher rd = request.getRequestDispatcher("/user");
//            rd.forward(request, response);
            HttpSession session = request.getSession();
            session.setAttribute("email", username);
            session.setAttribute("role", "USER");
            response.sendRedirect("/user");
        } else {
            if(userService.validateLogin(username, password).equals("ADMIN")){
                 HttpSession session = request.getSession();
                 session.setAttribute("email", username);
                 session.setAttribute("role", "ADMIN");
                 response.sendRedirect("/admin");
            }else {
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
                PrintWriter out = response.getWriter();
                if(username != null || password != null){
                    out.println("<font color=red>Either user name or password is wrong.</font>");
                }
                rd.include(request, response);
            }
        }

    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String html = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader("D:\\Users\\andam\\Documents\\Anul IV\\DS\\DS2018_MoldovanAnda_Assignment1\\presentation\\src\\main\\login.html"));
            String str;
            while((str = br.readLine()) != null){
                html += str;
                System.out.println(str);
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        out.println(html);
        doPost(request, response);

    }
//    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
//        response.setContentType("text/html");
//
//        String username = request.getParameter("username");
//        String password = request.getParameter("password");
//
//        if(username.equals("andamold@gmail.com") && password.equals("pass")){
//            response.sendRedirect("user.html");
//        }
//    }
}
