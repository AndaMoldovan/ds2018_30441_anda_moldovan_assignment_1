package services;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.List;
import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CityService {

    private final CityDAO cityDAO;

    public CityService(CityDAO cityDAO){this.cityDAO = cityDAO;}

    public String[] getCity(String lat, String lng){
        String[] result = new String[1];
        int i = 0;
        try{
            URL url = new URL("http://api.geonames.org/timezoneJSON?lat="+ lat +"&lng="+ lng +"&username=andamold");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                result[i] = output;
                i++;
            }

            conn.disconnect();
            return result;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch(IOException f){
            f.printStackTrace();
        }
        return null;
    }

    public void addCity(String lat, String lng, String cityName){
        String[] result = getCity(lat, lng);
        JsonParser parser = new JsonParser();
        JsonObject rootObj = parser.parse(result[0]).getAsJsonObject();

        City newCity = new City();
        newCity.setLatitude(rootObj.get("lat").toString());
        newCity.setLongitude(rootObj.get("lng").toString());
        newCity.setName(cityName);
        newCity.setGmtOffset(rootObj.get("gmtOffset").toString());
        newCity.setDstOffset(rootObj.get("dstOffset").toString());
        newCity.setCountry(rootObj.get("countryName").toString());

        cityDAO.addCity(newCity);
        //System.out.println(newCity.toString());
    }

    public void deleteCity(int id){
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        List<Flight> flights = flightDAO.getAllFlights();
        for(Flight flight : flights){
            if(flight.getArrivalCity().getId() == id || flight.getDepartureCity().getId() == id){
                flightDAO.deleteFlight(flight.getId());
            }
        }
        cityDAO.deleteCity(id);
    }
}
