package services;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightService {


    public FlightService(){

    }


    public String[] getDateAndTime(String date){
        String[] result = new String[2];

        result[0] = date.substring(0, 10);
        result[1] = date.substring(11, 19);

        return result;
    }

    public String calculateArrivalTime(String departureDate, String departureTime, String flightTime, String gmt, String dst){

        String day = departureDate.split("-")[2];
        String month = departureDate.split("-")[1];

        String arrivalTime;

        if(Integer.parseInt(month) > 1 && Integer.parseInt(day) > 1){
            int arrivalHour = (Integer.parseInt(departureTime.split(":")[0]) + Integer.parseInt(flightTime.split(":")[0]) + Integer.parseInt(gmt));
            int arrivalMinute = Integer.parseInt(departureTime.split(":")[1]) + Integer.parseInt(flightTime.split(":")[1]);

            if(arrivalMinute > 59){
                arrivalHour++;
                arrivalMinute = arrivalMinute - 60;
            }

            if(arrivalMinute == 0){
                arrivalTime = Integer.toString(arrivalHour) + ":0" + Integer.toString(arrivalMinute) + ":00";
            }else{
                arrivalTime = Integer.toString(arrivalHour) + ":" + Integer.toString(arrivalMinute) + ":00";
            }
            return arrivalTime;
        }else{
            int arrivalHour = Integer.parseInt(departureTime.substring(0, 2)) + Integer.parseInt(flightTime.split(":")[0]) + Integer.parseInt(dst);
            int arrivalMinute = Integer.parseInt(departureTime.split(":")[1]) + Integer.parseInt(flightTime.split(":")[1]);
            if(arrivalMinute > 59){
                arrivalHour++;
                arrivalMinute = arrivalMinute - 60;
            }
            if(arrivalMinute == 0){
                arrivalTime = Integer.toString(arrivalHour) + ":0" + Integer.toString(arrivalMinute) + ":00";
            }else{
                arrivalTime = Integer.toString(arrivalHour) + ":" + Integer.toString(arrivalMinute) + ":00";
            }
            return arrivalTime;
        }

    }

    public void createFlight(String departureCityName ,String departureDate, String departureTime, String arrivalCityName, String flightTime, String airplaneType, String flightNumber) throws ParseException {
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());

        List<City> cities = cityDAO.getAllCities();
        City arrivalCity = new City();
        City departureCity = new City();

        for(City city : cities){
            if(city.getName().equals(departureCityName)){
//                String countryName = city.getCountry();
//                departureCity.setCountry(countryName);
                departureCity.setCountry(city.getCountry());
                departureCity.setDstOffset(city.getDstOffset());
                departureCity.setGmtOffset(city.getGmtOffset());
                departureCity.setName(departureCityName);
                departureCity.setLongitude(city.getLongitude());
                departureCity.setLatitude(city.getLatitude());
                departureCity.setId(city.getId());
            }
            if(city.getName().equals(arrivalCityName)){
                arrivalCity.setCountry(city.getCountry());
                arrivalCity.setDstOffset(city.getDstOffset());
                arrivalCity.setGmtOffset(city.getGmtOffset());
                arrivalCity.setName(arrivalCityName);
                arrivalCity.setLongitude(city.getLongitude());
                arrivalCity.setLatitude(city.getLatitude());
                arrivalCity.setId(city.getId());
            }
        }

        Flight flight = new Flight();
        if(arrivalCity.getId() != 0 && departureCity.getId() != 0){
            flight.setDepartureCity(departureCity);
            flight.setArrivalCity(arrivalCity);
            flight.setFlightNumber(flightNumber);
            flight.setAirplaneType(airplaneType);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
            Date date = (Date) formatter.parse(departureDate + " " + departureTime);
            flight.setDepartureDate(new Timestamp(date.getTime()));

            String time = calculateArrivalTime(departureDate, departureTime, flightTime, arrivalCity.getGmtOffset(), arrivalCity.getDstOffset());
            if(departureCity.getGmtOffset() == arrivalCity.getGmtOffset()){
                String[] oldTime = calculateArrivalTime(departureDate, departureTime, flightTime, arrivalCity.getGmtOffset(), arrivalCity.getDstOffset()).split(":");
                int hour = Integer.parseInt(oldTime[0]) - Integer.parseInt(arrivalCity.getGmtOffset());
                String newTime = hour + ":" + oldTime[1] + ":" + oldTime[2];
                date = formatter.parse(newTime);
            }else{
                date = (Date) formatter.parse(departureDate + " " + calculateArrivalTime(departureDate, departureTime, flightTime, arrivalCity.getGmtOffset(), arrivalCity.getDstOffset()));

            }
            flight.setArrivalDate(new Timestamp(date.getTime()));

            flightDAO.addFlight(flight);
        }
    }

    public void updateFlight(int flightId, String departureCityName ,String departureDate, String departureTime, String arrivalCityName, String flightTime, String airplaneType, String flightNumber) throws ParseException {
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        Flight flight = flightDAO.getFlightById(flightId);

        if(!departureCityName.equals("") || !arrivalCityName.equals("")){
            CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
            List<City> cities = cityDAO.getAllCities();
            for(City city : cities){
                if(departureCityName.equals(city.getName())){
                    City newDepartureCity = cityDAO.getCityById(city.getId());
                    flight.setDepartureCity(newDepartureCity);
                }
                if(arrivalCityName.equals(city.getName())){
                    City newArrivalCity = cityDAO.getCityById(city.getId());
                    flight.setArrivalCity(newArrivalCity);
                }
            }

        }
        if(!flightTime.equals("") || !departureDate.equals("") || !departureTime.equals("")){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
            String newTime = calculateArrivalTime(departureDate, departureTime, flightTime, flight.getArrivalCity().getGmtOffset(), flight.getArrivalCity().getDstOffset());
            Date date = (Date) formatter.parse(departureDate + " " + newTime);
            flight.setArrivalDate(new Timestamp(date.getTime()));

            if(!departureDate.equals("") && !departureTime.equals("")){
                Date newDate = formatter.parse(departureDate + " " + departureTime);
                flight.setDepartureDate(new Timestamp(newDate.getTime()));
            }
        }
        if(!airplaneType.equals("")){
            flight.setAirplaneType(airplaneType);
        }
        if(!flightNumber.equals("")){
            flight.setFlightNumber(flightNumber);
        }
        flightDAO.updateFlight(flight);
        System.out.println("\n\n\n\n\n\n\n" + flight.getDepartureCity().getName());
    }

    public void deleteFlight(int id){
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        flightDAO.deleteFlight(id);
    }
}
