package services;

import dao.UserDAO;
import dao.UserRoleDAO;
import entities.User;

import java.util.List;

public class UserService {

    private final UserDAO userDAO;

    public UserService(UserDAO userDAO){ this.userDAO = userDAO; }

    public String validateLogin(String email, String password){
        List<User> users = userDAO.getAllUSers();

        for(User user : users){
            if(user.getEmail().equals(email) && user.getPassword().equals(password)){
                return user.getRoleId().getRole();
            }
        }
        return "false";
    }
}
