
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class UserServlet extends HttpServlet{

    private String message;

    public void init(){
        message = "Hello User";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String html = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader("D:\\Users\\andam\\Documents\\Anul IV\\DS\\DS2018_MoldovanAnda_Assignment1\\presentation\\src\\main\\user.html"));
            String str;
            while((str = br.readLine()) != null){
                html += str;
              //  System.out.println(str);
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        out.println(html);

        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        List<Flight> flights = flightDAO.getAllFlights();


        String table = "<table data-toggle=\"table\" style=\"padding-left: 100px; padding-top: 50px;\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Flight Number</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Departure City</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Departure Date</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Arrival City</th>\n" +
                "      <th scope=\"col\" style=\" padding-left: 20px; padding-right: 20px; border: 1px solid red;\">Arrival Date</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n";


        for(Flight flight : flights){
            table += "<tr>";
            table += "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getFlightNumber() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getDepartureCity().getName() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getDepartureDate() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getArrivalCity().getName() + "</td>\n" +
                    "<td style=\"border: 1px solid black; padding-left: 50px;\">" + flight.getArrivalDate() + "</td>\n";
            table += "</tr>";
        }
        table += "</tbody>\n" +
                "</table>";

        out.println(table);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("submit").equals("logout")){
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("/login");
        }
        doGet(request, response);
    }

}
